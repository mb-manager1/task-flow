export function isArray(o){
	return Array.isArray(o)
}

export function isObject(value) {
	const type = typeof value;
	return value != null && (type === 'object' || type === 'function');
}