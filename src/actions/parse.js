import Parser from "../parse";
import {isObject} from "../helpers/is";
//import utils from 'util'


export default async (cli, file, options = {}) => {
	const parser = new Parser({
		error: cli.error
	});
	await parser.readFile(file);
	// console.log(cli.chalk.red("--------------------- contents---------------------------") );
	// console.log(utils.inspect(parser.content, true, 5));
	await parser.parse();
	// console.log(cli.chalk.red("--------------------- flow ------------------------------") );
	// console.log(utils.inspect(parser.flow, true, 5));
	// console.log(cli.chalk.red("--------------------- errors ------------------------------") );
	// console.log(parser.parseErrors);
	// console.log(cli.chalk.red("------------------------------------------------------------") );

	function space(off){
		return (new Array(off)).fill('\t').join('')
	}
	console.log('');
	console.log(cli.chalk.blue.bold('Items'));
	for( let itemName in parser.flow.items ){
		let item = parser.flow.items[itemName];
		console.log(cli.chalk.green(`--->${itemName}`));
		console.log(space(1), cli.chalk.cyan('Type'), item.type);

		if(item.markup) {
			console.log(space(1), cli.chalk.cyan('Markup'), item.markup);
		}
	}
	console.log('');
	console.log(cli.chalk.blue.bold('States'));

	for( let stateName in parser.flow.states ) {
		let item = parser.flow.states[stateName];
		console.log(cli.chalk.green(`--->${stateName}`));
		console.log(space(1), cli.chalk.cyan('Name'), item.name );
		console.log(space(1), cli.chalk.cyan('Version'), item.version );
	}
	console.log('');
	console.log(cli.chalk.blue.bold('Transitions'));
	for( let itemName in parser.flow.items ) {
		let item = parser.flow.items[itemName];
		console.log(cli.chalk.green(`------->${itemName}`));
		item.transitions.forEach((transitionRow) => {
			console.log(space(1), cli.chalk.cyan('From'), transitionRow.from);
			console.log(space(1), cli.chalk.cyan('To'), transitionRow.to);
			console.log(space(1), cli.chalk.cyan('Actions:'));
			transitionRow.actions.forEach(( actionRow, actionIndex) => {
				Object.keys(actionRow).forEach((actionKey) => {
					if(isObject(actionRow[actionKey])){
						console.log(space(2), cli.chalk.cyan(actionKey));
						Object.keys(actionRow[actionKey]).forEach((descKey, descIndex) => {
							console.log(space(2), cli.chalk.cyan(descKey), actionRow[actionKey][descKey]);
						});
					} else {
						console.log(space(2), cli.chalk.cyan(actionKey), actionRow[actionKey]);
					}
				});
			})
		})
	}

	console.log('');
	console.log(cli.chalk.blue.bold('Views'));
	console.log(cli.chalk.blue.bold('--->Item views'));
	for( let itemName in parser.flow.items ){
		let item = parser.flow.items[itemName];

		for( let viewKey in item.views) {
			console.log(cli.chalk.green(`------->${itemName}->${viewKey}`));

			if(item.views[viewKey].rows){
				item.views[viewKey].rows.forEach(fieldRow=>{
					console.log(space(1), cli.chalk.cyan('Type'), fieldRow.type+(fieldRow.typeFormat?` (${fieldRow.typeFormat})`:''));
					console.log(cli.chalk.cyan('Initial Value'), fieldRow.initialValue?fieldRow.initialValue:'');
					console.log(cli.chalk.cyan('Validation'));
					if(fieldRow.validation ){
						Object.keys(fieldRow.validation).forEach((key) => {
							console.log(space(1), `${key} :`);
							Object.keys(fieldRow.validation[key]).forEach((subkey)=>{
								console.log(space(2), `${subkey}: ${fieldRow.validation[key][subkey]}`);
							});
						})
					}
				})
			}
		}
	}

	console.log('');
	console.log(cli.chalk.blue.bold('Boards'));
	for( let boardKey in parser.flow.boards ){
		let board = parser.flow.boards[boardKey];
		console.log(cli.chalk.green(`------->${board.name}`));

		let lines = new cli.ui.Line();
		let headers = new cli.ui.Line();

		board.columns.forEach((column) => {
			headers.column(column.header, 40, [cli.chalk.cyan]);
			lines.column(column.state ? column.state.name : '' , 40);
		});
		headers.fill().output();
		lines.fill().output();
	}

	console.log(cli.chalk.blue('--------------------------Errors'));
	parser.parseErrors.forEach((err) => {
		console.log(cli.chalk.red(err));
	});
}