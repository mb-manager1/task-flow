import clear from 'clear'
import clui from 'clui'
import figlet from 'figlet'
import chalk from 'chalk'

import yargs from 'yargs'

import actions from './actions'

const cli = {
	ui: clui,
	chalk,
	clear,
	warning: function() {
		const args = Array.prototype.slice.call(arguments).map( item => {
			return chalk.yellow(item);
		});
		console.log.apply(console, args)
	},
	error: function() {
		const args = Array.prototype.slice.call(arguments).map( item => {
			return chalk.red(item);
		});
		console.log.apply(console, args);
		process.exit()
	}

};

if (require.main === module) {
	clear();
	console.log(chalk.green(
		figlet.textSync('MB-Manager Flow', {
			horizontalLayout: 'full'
		})
	));

	yargs.command('parse [file]', 'parse flow file and preview', (args) => {
		args.positional('file', {
			describe: 'location of '
		});
	}, async (argv) => {

		await actions.parse(cli, argv.file, {})
	}).command('board', 'board options', (args) => {
		args.command('create', 'create board', (args) => {
			console.log("====")
		});
	}).argv;


	//process.stdout.write(hello());
}