import fs from 'fs'
import mime from 'mime-types'
import YAML from 'yaml'
import { isArray } from "../helpers/is";


function isVersionValid (version) {
	return parseFloat(version) > 1;
}

export default class Parser {
	constructor(output){
		this.rawContent = null;
		this.content = null;
		this.output = output;
		this.parseErrors = [];
		this.flow = {
			states: {},
			items: {},
			boards: {}
		};
	}
	async readFile( file ) {
		if (fs.existsSync(file) ) {
			if (mime.lookup(file) !== "text/yaml") {
				this.output.error('Not a yaml file')
			}
			this.rawContent = fs.readFileSync(file, 'utf8');
			this.content = YAML.parse(this.rawContent);
		}
	}
	async parse(){

		if (this.content) {
			const rootKeys = Object.keys(this.content);

			// read states
			rootKeys.forEach( (key) => {
				const rootItem = this.content[key];
				if( rootItem.type && rootItem.type === "state") {
					const state = {
						version: 1.0,
						unique_key: key
					};

					if(rootItem.version && isVersionValid(rootItem.version) ) {
						state.version = rootItem.version;
					}
					if(rootItem.name){
						state.name = rootItem.name;
					} else {
						this.parseErrors.push(`State: ${key} is missing name`);
					}
					this.flow.states[state.unique_key] = state
				}
			});

			//read items
			rootKeys.forEach( (key) => {
				const rootItem = this.content[key];
				if(rootItem.type && rootItem.type === 'item'){
					const item = {
						version: 1.0,
						unique_key: key,
						fields: {}
					};
					if(rootItem.version && isVersionValid(rootItem.version) ) {
						item.version = rootItem.version;
					}
					if(rootItem.name){
						item.name = rootItem.name;
					} else {
						this.parseErrors.push(`Item: ${key} is missing name`);
					}
					if(rootItem['initial-state'] ){
						if(this.flow.states[rootItem['initial-state']]) {
							item.state = this.flow.states[rootItem['initial-state']];
						} else {
							this.parseErrors.push(`Item: ${key} has missing initial state : ${rootItem['initial-state']}`)
						}
					}

					if(rootItem.fields){
						for( let fieldKey in rootItem.fields ){
							let fieldRules = rootItem.fields[fieldKey];
							if(!isArray(fieldRules)){
								this.parseErrors.push(`Item: ${key} has invalid field rules`);
								continue
							}
							let field = {
								type: null,
								transitions: []
							};
							let rules = {};
							fieldRules.forEach( (fieldRule) => {
								for( let i in  fieldRule){
									rules[i] = fieldRule[i]
								}
							});

							if (rules.type) {
								if( rules.type === 'string') {
									field.type = rules.type;
									if(rules.max_length){
										field.len = rules.max_length
									}
								} else if( rules.type === 'text' ){
									field.type = rules.type;
									field.markup = 'markdown';

									if(rules.markup){
										if(rules.markup === 'markdown'){
											field.markup = 'markdown'
										} else {
											vparseErrors.push(`Item: ${key} field ${fieldKey} has unsupported markdown : ${rules.markup}`)
										}
									}
								} else {
									this.parseErrors.push(`Item: ${key} has unsupported field type for ${fieldKey}`)
								}
							} else {
								this.parseErrors.push(`Item: ${key} has no field type for ${fieldKey}`)
							}
							this.flow.items[item.unique_key] = field;
						}

					} else {
						this.parseErrors.push(`Item: ${key} has missing fields`)
					}
				}
			});

			if(this.content.views){
				if(this.content.views.items){
					for( let itemTypeKey in this.content.views.items ){
						let itemType = this.content.views.items[itemTypeKey]
						if(this.flow.items[itemTypeKey]) {
							this.flow.items[itemTypeKey].views = {};

							Object.keys(itemType).forEach( viewKey => {
								let viewRow = itemType[viewKey];
								let view = {
									name: viewKey,
									rows: []
								};
								if(viewRow.version){
									view.version = viewRow.version
								}
								if(viewRow.components){
									viewRow.components.forEach( (viewFieldRow, viewFieldIndex) => {
										let viewItem = {};
										if(viewFieldRow.type){
											let parts = viewFieldRow.type.split(':');
											if(parts[0] === 'field') {
												viewItem.type = 'field';
											}
											if(parts[1] === 'input' || parts[1] === 'textarea'){
												viewItem.typeFormat = parts[1]
											}

											if(viewFieldRow.initial) {
												if(viewFieldRow.initial === 'empty'){
													viewItem.initialValue = ''
												}
												if(viewFieldRow.initial.split(':')[0] === 'db'){
													viewItem.fetch = true;
													viewItem.onFetch = `initialValue eq ${viewFieldRow.initial.split(':')[1]}`
												}
											}
											if(viewFieldRow.rules){
												viewItem.validation = {
													preSave: {}
												};
												const parseRule = (rule) => {
													let validationRule = {}
													if(rule.type === 'string'){
														validationRule.type = rule.type;
													}
													if(rule.min_length) {
														validationRule.min = rule.min_length
													}
													if(rule.max_length) {
														validationRule.max = rule.max_length
													}
													return validationRule;
												}
												if(viewFieldRow.rules.preSave){
													viewItem.validation.preSave = parseRule(viewFieldRow.rules.preSave)
												}
											}
											view.rows.push(viewItem)
										} else {
											this.parseErrors.push(`Item view: ${itemTypeKey}->${viewKey}[${viewFieldIndex}] has now type`)
										}
									})
								}
								this.flow.items[itemTypeKey].views[view.name] = view;
							})
						} else {
							this.parseErrors.push(`Item view: ${itemTypeKey} does not exist`)
						}
					}
				}

				if(this.content.views.boards) {
					Object.keys(this.content.views.boards).forEach((boardKey) => {
						let boardRow = this.content.views.boards[boardKey];
						if(boardRow.name){
							let board = {
								name: boardRow.name,
								version: boardRow.version || 1.0,
								columns: []
							};
							if(boardRow.columns){
								Object.keys(boardRow.columns).forEach((columnKey) => {
									let columnRow = boardRow.columns[columnKey];
									let boardColumn = {
										header: columnRow.header || ''
									};

									if(columnRow.state && this.flow.states[columnRow.state] ) {
										boardColumn.state = this.flow.states[columnRow.state];
									} else if(columnRow.state && !this.flow.states[columnRow.state]) {
										this.parseErrors.push(`State is missing in board ${boardKey} for column ${columnKey}`);
									} else {
										this.parseErrors.push(`Missing state in board ${boardKey} for column ${columnKey}`);
									}
									board.columns.push(boardColumn);
								});
							} else {
								this.parseErrors.push(`Missing board columns for key ${boardKey}`);
							}

							this.flow.boards[boardKey] = board;
						} else {
							this.parseErrors.push(`Missing board name for key ${boardKey}`);
						}

					})
				} else {
					this.parseErrors.push(`Missing board views`);
				}
			} else {
				this.parseErrors.push(`Missing views`);
			}

			if(this.content.transitions){
				this.content.transitions.forEach((transitionRow, transitionIndex) => {

					if( !transitionRow.from ) {
						this.parseErrors.push(`Transition[${transitionIndex}] is missing from state`);
					}

					if( !transitionRow.to ) {
						this.parseErrors.push(`Transition[${transitionIndex}] is missing to state`);
					}

					if( transitionRow.from && transitionRow.to ){
						let transition = {
							from: transitionRow.from,
							to: transitionRow.to,
							actions: []
						};

						transitionRow.actions.forEach( (actionRow, actionIndex) => {
							if(actionRow.change_state ){
								if(this.flow.states[actionRow.change_state]) {
									let action = {
										state: this.flow.states[actionRow.change_state]
									};
									transition.actions.push(action)
								} else {
									this.parseErrors.push(`Transition ${transitionRow.from} -> ${transitionRow.to} action[${actionIndex}] is changing to missing state: ${actionRow.change_state}`)
								}
							}
						});
						this.flow.items[transitionRow.item].transitions.push(transition);
					}
				});
			} else {
				this.parseErrors.push(`Missing transitions`)
			}
		} else {
			this.output.error('File not found')
		}
	}
}